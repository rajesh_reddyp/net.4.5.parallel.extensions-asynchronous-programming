﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace SimpleTasks
{
    public static class Tasks
    {
        public static void GetTaskResult(char[] delimiters, string headerText)
        {
            var task1 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("starting first task");
                var client = new WebClient();
                client.Headers.Add("user-agent", headerText);
                var words = client.DownloadString(@"http://www.gutenberg.org/files/2009/2009.txt");
                var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                return wordsArray.Count();
            });
            var task2 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("starting second task");
                var client = new WebClient();
                client.Headers.Add("user-agent", headerText);
                var words = client.DownloadString(@"http://www.gutenberg.org/files/16328/16328-8.txt");
                var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                return wordsArray.Count();
            });
            var task3 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("starting third task");
                var client = new WebClient();
                client.Headers.Add("user-agent", headerText);
                var words = client.DownloadString(@"http://www.gutenberg.org/files/4300/4300.txt");
                var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                return wordsArray.Count();
            });
            Console.WriteLine("Task1 Completed.Origin of Species word count:{0}", task1.Result);
            Console.WriteLine("Task2 Completed.Beowulf word count:{0}", task2.Result);
            Console.WriteLine("Task3 Completed.Ulysses of Species word count:{0}", task3.Result);
            Console.WriteLine("Please <Enter> Exit ");
            Console.ReadLine();
        }

        public static void PassDataToTask(char[] delimiters, string headerText)
        {
            var dictionary = new Dictionary<string, string> { 
               {"Origin of Species","http://www.gutenberg.org/files/2009/2009.txt"},
               {"Beowulf", "http://www.gutenberg.org/files/16328/16328-8.txt"},
               {"Ulysses", "http://www.gutenberg.org/files/4300/4300.txt"}
              };
            var tasks = new List<Task>();
            foreach(var pair in dictionary)
            {
                tasks.Add(Task.Factory.StartNew((stateobj) => {
                    var taskData = (KeyValuePair<string, string>)stateobj;
                    var client = new WebClient();
                    client.Headers.Add("user-agent", headerText);
                    var words = client.DownloadString(taskData.Value);
                    var wordArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    Console.WriteLine("Word count for {0}: {1}", taskData.Key,wordArray.Count());
                },pair));
            }
            Task.WaitAll(tasks.ToArray());
            Console.WriteLine("Press <Enter> to exit");
            Console.ReadLine();
        }
    }
}
