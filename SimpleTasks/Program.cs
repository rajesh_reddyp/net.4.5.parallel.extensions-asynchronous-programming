﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace SimpleTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            // starting a new thread
            char[] delimiters = { ' ', ',', '.', ';', ':', '-', '_', '/', '\u000A' };
            const string headerText = "Mozilla/5.0 (compatible;MSIE 10.0;Windows NT 6.1;Trident/6.0)";
            //Parallel.Invoke(() =>
            //{
            //    Console.WriteLine("Starting First Task Using  Parallel.Invoke");
            //    var client = new WebClient(); client.Headers.Add("user-agent", headerText);
            //    var words = client.DownloadString(@"http://www.gutenberg.org/files/2009/2009.txt");
            //    var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //    Console.WriteLine("Origin of Species word count:{0}", wordsArray.Count());
            //    client.Dispose();
            //});
            //var secondTask = new Task(() =>
            //{
            //    Console.WriteLine("Starting second task using Task.Start");
            //    var client = new WebClient(); client.Headers.Add("user-agent", headerText);
            //    var words = client.DownloadString(@"http://www.gutenberg.org/files/16328/16328-8.txt");
            //    var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //    Console.WriteLine("Beowulf word count:{0}", wordsArray.Count());
            //    client.Dispose();
            //});
            //secondTask.Start();
            //Task.Factory.StartNew(() =>
            //{
            //    Console.WriteLine("Starting third task using Task.Factory.StartNew");
            //    var client = new WebClient(); client.Headers.Add("user-agent", headerText);
            //    var words = client.DownloadString(@"http://www.gutenberg.org/files/4300/4300.txt");
            //    var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //    Console.WriteLine("Ulysses of Species word count:{0}", wordsArray.Count());
            //    client.Dispose();
            //});
            //Console.ReadLine();

            // waiting tasks
            //Wait.WaitingTask(delimiters, headerText);
            
            // returning results from task
           //Tasks.GetTaskResult(delimiters, headerText);

            // passing data to task
            Tasks.PassDataToTask(delimiters, headerText);
        }
    }
}
