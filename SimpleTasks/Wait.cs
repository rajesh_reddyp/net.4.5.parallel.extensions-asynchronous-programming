﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTasks
{
    public static class Wait
    {
        public static void WaitingTask(char[] delimiters,string headerText)
        { 
          var task1 = Task.Factory.StartNew(() => {
              Console.WriteLine("starting first task");
              var client = new WebClient();
              client.Headers.Add("user-agent",headerText);
              var words = client.DownloadString(@"http://www.gutenberg.org/files/2009/2009.txt");
              var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
              Console.WriteLine("Origin of Species word count:{0}", wordsArray.Count());
          });
          task1.Wait();
          Console.WriteLine("Task 1 complete. Creating Task 2 and Task 3.");
          var task2 = Task.Factory.StartNew(() =>
          {
              Console.WriteLine("starting second task");
              var client = new WebClient();
              client.Headers.Add("user-agent", headerText);
              var words = client.DownloadString(@"http://www.gutenberg.org/files/16328/16328-8.txt");
              var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
              Console.WriteLine("Beowulf word count:{0}", wordsArray.Count());
          });
          var task3 = Task.Factory.StartNew(() =>
          {
              Console.WriteLine("starting third task");
              var client = new WebClient();
              client.Headers.Add("user-agent", headerText);
              var words = client.DownloadString(@"http://www.gutenberg.org/files/4300/4300.txt");
              var wordsArray = words.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
              Console.WriteLine("Ulysses of Species word count:{0}", wordsArray.Count());
          });
          Task.WaitAll(task2,task3);
          Console.WriteLine("All tasks completed...");
          Console.WriteLine("press <enter> to exit");
          Console.ReadLine();
        }

    }
}
